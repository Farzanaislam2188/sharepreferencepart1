package com.example.user.sharepreference;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText editTextUserName,editTextPassword;
    Button buttonsave,buttonload;
    TextView textViewdetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewdetails=findViewById(R.id.textViewdetails);
        editTextPassword=findViewById(R.id.editTextPassword);
        editTextUserName=findViewById(R.id.editTextUserName);
        buttonload=findViewById(R.id.buttonload);
        buttonsave=findViewById(R.id.buttonsave);
        buttonsave.setOnClickListener(this);
        buttonload.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.buttonsave) {

            String username=editTextUserName.getText().toString();
            String password=editTextPassword.getText().toString();
            if(username.equals("") && password.equals("")) {
                Toast.makeText(getApplicationContext(),"please enter your information",Toast.LENGTH_LONG).show();
            }
            else {
                SharedPreferences sharedPreferences = getSharedPreferences("user details", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("usernamekey", username);
                editor.putString("passwordkey", password);
                editor.commit();
                editTextPassword.setText("");
                editTextUserName.setText("");
                Toast.makeText(getApplicationContext(),"log in successfully",Toast.LENGTH_LONG).show();
                Intent intent=new Intent(MainActivity.this,loginpage.class);
                startActivity(intent);
            }
        }
        else if(view.getId()==R.id.buttonload)
        {
SharedPreferences sharedPreferences=getSharedPreferences("user details",Context.MODE_PRIVATE);
if(sharedPreferences.contains("usernamekey") && sharedPreferences.contains("passwordkey")) {
String username=sharedPreferences.getString("usernamekey","data not found");
String password=sharedPreferences.getString("passwordkey","data not found");
    textViewdetails.setText(username+"\n"+password);
}
        }
        }

    }

