package com.example.user.sharepreference;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class loginpage extends AppCompatActivity implements View.OnClickListener {
    TextView tv;
    Button buttonid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginpage);
        tv=findViewById(R.id.tv);
        buttonid=findViewById(R.id.buttonid);
        buttonid.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        Intent intent=new Intent(loginpage.this,MainActivity.class);
        startActivity(intent);
        Toast.makeText(getApplicationContext(),"log out successfully",Toast.LENGTH_LONG).show();

    }
}
